.. _reproducible-date(1):

=================
reproducible-date
=================

:Manual section: 1

Synopsis
========

reproducible-date [DATE-ARGS]


Description
===========

``reroducible-date`` inspects the ``SOURCE_DATE_EPOCH`` [0] environment
variable and formats a date from this value. The locale and timezone are
fixed to ``LC_ALL=C`` and UTC and respectfully.

If the ``SOURCE_DATE_EPOCH`` environment variable is not specified, a warning
is printed and the current time and existing locale timezone are used instead.


Command line arguments
======================

All arguments are passed directly to ``date(1)``.


History
=======

reproducible-date was written by Chris Lamb <lamby@debian.org>.
