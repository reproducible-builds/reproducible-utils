.. _clamp-mtimes(1):

============
clamp-mtimes
============

:Manual section: 1

Synopsis
========

clamp-mtimes [FIND-OPTIONS]


Description
===========

``clamp-mtimes`` inspects the ``SOURCE_DATE_EPOCH`` [0] environment variable
and updates the modification times of the specified files to this value, but
only in the case that files existing modification times' are newer than this
value.

If the ``SOURCE_DATE_EPOCH`` environment variable is not specified, a warning
is printed and the current time is used.


Command line arguments
======================

All command-line options are passed directly to ``find(1)``.


References
==========

 [0] https://reproducible-builds.org/specs/source-date-epoch/


History
=======

clamp-mtimes was written by Chris Lamb <lamby@debian.org>.
