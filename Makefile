SHELL := sh -e

CLEAN = man/
SCRIPTS = $(wildcard bin/*)
MANPAGES = $(patsubst rst/%.rst,man/%.1,$(wildcard rst/*.rst))

all: $(MANPAGES)

man/%.1: rst/%.rst
	mkdir -p $$(dirname $@)
	rst2man $< $@ 

install: all
	for X in $(SCRIPTS); \
	do \
		install -D -m 0755 $${X} $(DESTDIR)/usr/bin/$$(basename $${X}); \
	done
	
	for X in $(MANPAGES); \
	do \
		install -D -m 0644 $${X} $(DESTDIR)/usr/share/man/man1/$$(basename $${X}); \
	done

test:
	@if [ -x /usr/bin/shellcheck ]; \
	then \
		for X in $(SCRIPTS); \
		do \
			echo -n "Checking $${X} for bashisms... "; \
			shellcheck $${X}; \
			echo "ok"; \
		done; \
	else \
		echo "W: skipping bashisms tests - you need to install shellcheck."; \
	fi

clean:
	rm -rf $(CLEAN)

distclean: clean
